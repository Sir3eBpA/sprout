﻿using UnityEngine;
using System.Collections;

public class Chunk : MonoBehaviour {

    int chunkSize;

    void OnDrawGizmos()
    {
        chunkSize = GameObject.FindGameObjectWithTag("Interface").GetComponent<MenuInterface>().chunkSize;
        Gizmos.color = Color.white;
        Gizmos.DrawWireCube(transform.position, new Vector3(chunkSize, chunkSize, 0));
    }
}
