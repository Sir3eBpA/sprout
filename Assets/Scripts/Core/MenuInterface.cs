﻿using System;
using UnityEngine;

public class MenuInterface : MonoBehaviour
{
    //<Player settings>
    public int playerHealth;
    public float playerSpeed;
    public float playerJumpPower;
    //</Player settings>


    //<Camera settings>
    public Vector2 cameraSmoothness;
    public Vector2 cameraOffset;
    //</Camera settings>

    //<Chunk settings>
    public int chunkSize;
    //</Chunk settings>

    //<Player Controller Settings>
    public int verticalRayCount;
    public int horizontalRayCount;
    //</Player Controller Settings>

    void Start()
    {
        Debug.Log(String.Format("{0} (sec);Interface Initialised", Time.realtimeSinceStartup));
    }
}

