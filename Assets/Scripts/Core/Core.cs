﻿using System;
using UnityEngine;

class Core : MonoBehaviour
{
    PlayerActor playerActor;
    MenuInterface mInterface;
    CameraFollow playerCamera;

    ChunkManager chunkManager;

    GameObject playerObject;

    void Start()
    {
        Debug.Log(String.Format("{0} (sec);Core started", Time.realtimeSinceStartup));
        mInterface = GameObject.FindGameObjectWithTag("Interface").GetComponent<MenuInterface>();

        playerObject = GameObject.FindGameObjectWithTag("Player");

        playerActor = new PlayerActor(
            mInterface.playerHealth,
            mInterface.playerSpeed,
            mInterface.playerJumpPower,
            playerObject,
            mInterface.verticalRayCount,
            mInterface.horizontalRayCount
        );

        playerCamera = new CameraFollow(
            mInterface.cameraSmoothness,
            mInterface.cameraOffset,
            playerObject
        );

        chunkManager = new ChunkManager();

    }

    void Update()
    {
        playerCamera.followPlayer();

        playerActor.handleControl();
    }

    void LateUpdate()
    {
        chunkManager.handleChunkUpdates(playerObject.transform.position);
    }
}

