﻿Shader "Custom/WaterReflection" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_Amplitude ("Sine Amplitude", Float) = 0.5
		_Speed ("Speed in seconds", Float) = 0.2
		_Count ("Count of sine waves (less eq. smoother anim)", Range(0.0,1.0)) = 0.05
		[PerRendererData]_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader {
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha 
		Lighting Off
		Cull Off

		Tags { "RenderType"="Opaque" }
		
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			float4 _Color;
			float _Amplitude;
			float _Speed;
			float _Count;
			sampler2D _MainTex;

			float4 _MainTex_ST;

			struct v2f
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			v2f vert(v2f IN)
			{
				v2f OUT;
				OUT.vertex = mul(UNITY_MATRIX_MVP, IN.vertex);
				OUT.uv = IN.uv;

				return OUT;
			}
	
			float4 frag(v2f IN) : COLOR
			{
				const float PI = 3.14159265359;
				float2 offset = IN.uv;

				float factor = (2 * PI) / _Speed;
				/*
					calculating speed of our sine wave
					(foward and backward moves)
				*/
				
				offset.x += (sin(offset.y * _Amplitude + factor * _Time) * _Count);
				/*
					implementing sine wave offset
					based on amplitude and factor
				*/

				return tex2D(_MainTex, offset) * _Color;
			}

			ENDCG
		}
	}
	FallBack "Diffuse"
}
