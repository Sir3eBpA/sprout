﻿using System;
using UnityEngine;

namespace Assets.Scripts.Core
{
    abstract class Actor
    {
        int health;
        float speed;
        float jumpPower;

        bool facingRight = true;

        protected GameObject body;
        protected SpriteRenderer sprite;
        protected BoxCollider2D collider;

        protected RaycastOrigins rayOrigins;
        protected collision collision;

        protected Vector2 velocity;

        protected float gravity;
        protected float jumpVelocity;

        int verticalRayCount;
        int horizontalRayCount;
        float horizontalSpacing;
        float verticalSpacing;

        public GameObject Body
        {
            get { return body; }
        }

        public float JumpPower
        {
            get { return jumpPower; }
            set
            {
                if (value > 0) jumpPower = value;
                else throw new ArgumentOutOfRangeException();
            }
        }

        public int Health
        {
            get { return health; }
            set
            {
                if (value > 0 && (health - value) > 0)
                    health -= value;
                else if (value > 0 && (health - value) < 0)
                    health = 0;
                else throw new ArgumentOutOfRangeException();
            }
        }

        public float Speed
        {
            get { return speed; }
            set
            {
                if (value > 0) speed = value;
                else throw new ArgumentOutOfRangeException();
            }
        }

        public Actor(
            int health,
            float speed,
            float jumpPower,
            GameObject body,
            int verticalRayCount,
            int horizontalRayCount)
        {
            this.health = health;
            this.speed = speed;
            this.jumpPower = jumpPower;

            this.body = body;

            this.sprite = this.body.GetComponent<SpriteRenderer>();
            this.collider = this.body.GetComponent<BoxCollider2D>();

            this.body.layer = 2;

            this.verticalRayCount = verticalRayCount;
            this.horizontalRayCount = horizontalRayCount;

            Debug.Log(String.Format("{0} (sec);sprite created", Time.realtimeSinceStartup));

            gravity = -(2 * jumpPower) / Mathf.Pow(0.4f, 2);
            jumpVelocity = Mathf.Abs(gravity) * 0.4f;

            CalculateRaySpacing();
        }

        /// <summary>
        /// Move actor with specific velocity
        /// </summary>
        /// <param name="velocity">Vector2D velocity</param>
        public void Move(Vector2 velocity)
        {
            UpdateRaycastOrigin();
            collision.Reset();

            if (velocity.x != 0)
                HorizontalCollision(ref velocity);
            if (velocity.y != 0)
                VerticalCollision(ref velocity);

            body.transform.Translate(velocity);
        }

        private void HorizontalCollision(ref Vector2 velocity)
        {
            float directionX = Mathf.Sign(velocity.x);
            float rayLength = Mathf.Abs(velocity.x);

            for (int i = 0; i < horizontalRayCount; i++)
            {
                Vector2 rayOrigin = (directionX == -1) ? rayOrigins.bottomLeft : rayOrigins.bottomRight;
                rayOrigin += Vector2.up * (horizontalSpacing * i + 0.1f);

                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength);

                Debug.DrawRay(rayOrigin, Vector2.right * directionX * rayLength, Color.red);

                if (hit)
                {
                    velocity.x = 0;

                    collision.left = (directionX == -1);
                    collision.right = (directionX == 1);
                }
            }
        }

        private void VerticalCollision(ref Vector2 velocity)
        {
            float directionY = Mathf.Sign(velocity.y);
            float rayLength = Mathf.Abs(velocity.y);

            for (int i = 0; i < verticalRayCount; i++)
            {
                Vector2 rayOrigin = (directionY == -1) ? rayOrigins.bottomLeft : rayOrigins.topLeft;
                rayOrigin += Vector2.right * (verticalSpacing * i + velocity.x);

                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * directionY, rayLength);

                Debug.DrawRay(rayOrigin, Vector2.up * directionY * rayLength, Color.red);

                if (hit)
                {
                    velocity.y = hit.distance * directionY;

                    collision.below = (directionY == -1);
                    collision.above = (directionY == 1);
                }

            }
        }

        /// <summary>
        /// When sprite goes to the opposite direction - flip it's sprite
        /// </summary>
        private void Flip()
        {
            facingRight = !facingRight;

            Vector3 spriteScale = sprite.transform.localScale;
            spriteScale.x *= -1;
            sprite.transform.localScale = spriteScale;
        }


        /// <summary>
        /// Decide whether we should flip our sprite's sprite
        /// or not, using Direction variable
        /// </summary>
        /// <param name="direction">Represents 1 or -1 value where sprite moves</param>
        public void SetSpriteDirection(int direction) // flips sprite
        {
            if (direction == 1 && !facingRight || direction == -1 && facingRight)
                Flip();
        }

        /// <summary>
        /// Get RayCast origin where start (on player collider)
        /// </summary>
        private void UpdateRaycastOrigin()
        {
            Bounds bounds = collider.bounds;

            rayOrigins.bottomLeft = new Vector2(bounds.min.x, bounds.min.y);
            rayOrigins.bottomRight = new Vector2(bounds.max.x, bounds.min.y);
            rayOrigins.topLeft = new Vector2(bounds.min.x, bounds.max.y);
            rayOrigins.topRight = new Vector2(bounds.max.x, bounds.max.y);
        }

        private void CalculateRaySpacing()
        {
            Bounds bounds = collider.bounds;

            verticalSpacing = bounds.size.x / (verticalRayCount - 1);
            horizontalSpacing = bounds.size.y / (horizontalRayCount);
        }
    }

    struct RaycastOrigins
    {
        public Vector2 topLeft, topRight;
        public Vector2 bottomLeft, bottomRight;
    }

    public struct collision
    {
        public bool above, below;
        public bool left, right;

        public void Reset()
        {
            above = below = false;
            left = right = false;
        }
    }
}
