﻿using UnityEngine;
using System.Collections;

public class ChunkManager {

	GameObject[] chunks;

    public readonly int chunkSize;

    public GameObject[] Chunks
    {
        get { return chunks; }
    }

    public ChunkManager()
    {
        getChunks();

        //get chunkSize from our visual interface
        chunkSize = GameObject.FindGameObjectWithTag("Interface").GetComponent<MenuInterface>().chunkSize;

        Debug.Log(string.Format("Chunk manager activated;\nchunk size : {0} \nchunks : {1} ",
            chunkSize,
            chunks.Length
            ));
    }

    /// <summary>
    /// Get all chunks into single GameObject[] array
    /// </summary>
    private void getChunks()
    {
        chunks = GameObject.FindGameObjectsWithTag("Chunk");
    }
    
    /// <summary>
    /// Use this to let script manipulate chunks
    /// and their position in order
    /// to correctly calculate and draw everything
    /// </summary>
    public void handleChunkUpdates(Vector2 playerPosition)
    {
        for (int i = 0; i < chunks.Length; i++)
            if (Vector2.Distance(playerPosition, chunks[i].transform.position) < (chunkSize))
                enableChunk(chunks[i]);
            else
                disableChunk(chunks[i]);
    }

    /// <summary>
    /// Disables chunks if it's out of Player's view
    /// </summary>
    private void disableChunk(GameObject chunk)
    {
        chunk.SetActive(false);
    }

    /// <summary>
    /// Enables chunks if it's in Player's view
    /// </summary>
    private void enableChunk(GameObject chunk)
    {
        chunk.SetActive(true);
    }
}
