﻿Shader "Effects/BloomAndBlur" {
    Properties {
        _Color ("Color", Color) = (1,1,1,1)
		_Brightness ("Brightness", Range(0.0,2)) = 0
		_Intensity ("Intensity", Range(0,0.025)) = 0.000
		[PerRendererData]_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader {
        Tags 
		{
            "Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True" 
		}
				
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha 
		Lighting Off
		Cull Off

		BindChannels 
		{
			Bind "Vertex", vertex
			Bind "texcoord", texcoord 
			Bind "Color", color 
		}

		Pass
		{
			CGPROGRAM
			#include "UnityCG.cginc"

			#pragma vertex vert
			#pragma fragment frag


			float4 _Color;
			float _Intensity;
			float _Brightness;
			sampler2D _MainTex;

			float4 _MainTex_ST;

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			v2f vert(appdata_base IN)
			{
				v2f OUT;
				OUT.vertex = mul(UNITY_MATRIX_MVP, IN.vertex);
				OUT.uv = TRANSFORM_TEX(IN.texcoord, _MainTex);

				return OUT;
			}

			half4 frag(v2f IN) : COLOR
			{
				half4 tex = half4(0.0,0.0,0.0,0.0);
				float2 uv = IN.uv;

				float coef = 1.0;
				float remaining = 1.0;

				for(int i = 0; i < 4; i++)
				{
					coef *= 0.36;

					tex += tex2D(_MainTex, float2(uv.x, uv.y - i * _Intensity)) * coef;
					tex += tex2D(_MainTex, float2(uv.x - i * _Intensity, uv.y)) * coef;
					tex += tex2D(_MainTex, float2(uv.x + i * _Intensity, uv.y)) * coef;
					tex += tex2D(_MainTex, float2(uv.x, uv.y + i * _Intensity)) * coef;

					remaining -= 4*coef;
				}
				tex += tex2D(_MainTex, uv) * remaining;
				
				if(_Brightness > 0.01)
					tex *= _Brightness + _Color;

				return tex * _Color;
			}


			ENDCG
		}
	}
}