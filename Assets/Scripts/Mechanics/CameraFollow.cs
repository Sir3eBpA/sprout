﻿using System;
using UnityEngine;

class CameraFollow
{
    private Vector2 velocity;
    private Vector2 pos;
    private Vector2 smoothness;
    private Vector2 offset;

    private GameObject player;

    public Vector2 Smoothness
    {
        get { return smoothness; }
        set
        {
            if (smoothness.x > 0 && smoothness.y > 0)
                smoothness = value;
            else
                throw new ArgumentOutOfRangeException();
        }
    }

    public CameraFollow(Vector2 smoothness, Vector2 offset, GameObject player)
    {
        this.smoothness = smoothness;
        this.offset = offset;

        this.player = player;
    }

    public void followPlayer()
    {
        pos.x = Mathf.SmoothDamp(
            Camera.main.transform.position.x,
            this.player.transform.position.x + offset.x,
            ref velocity.x,
            smoothness.x
        );

        pos.y = Mathf.SmoothDamp(
            Camera.main.transform.position.y,
            this.player.transform.position.y + offset.y,
            ref velocity.y,
            smoothness.y
        );

        Camera.main.transform.position = new Vector3(pos.x, pos.y, Camera.main.transform.position.z);
    }
}

