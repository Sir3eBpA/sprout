﻿#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;

class SelectAllOfTag : ScriptableWizard
{
    string tagName = "Chunk";

    [MenuItem("API Test/Select All of Tag...")]
    static void SelectAllOfTagWizard()
    {
        ScriptableWizard.DisplayWizard(
            "Select All of Tag...",
            typeof(SelectAllOfTag),
            "Make Selection");
    }

    void OnWizardCreate()
    {
        GameObject g = GameObject.FindGameObjectWithTag("Chunk");
        Transform[] gos = g.GetComponentsInChildren<Transform>();

        Selection.objects = gos;

        Debug.Log(gos.Length.ToString());
    }
}
#endif