﻿using System;
using UnityEngine;

using Assets.Scripts.Core;

class PlayerActor : Actor
{
    public PlayerActor(int health,
            float speed,
            float jumpPower,
            GameObject body,
            int verticalRayCount,
            int horizontalRayCount)
        : 
        base(health, speed, jumpPower, body, verticalRayCount, horizontalRayCount)
    {}

    /// <summary>
    /// This method is used to control all sprite's
    /// behaviour using Input keys (TODO : Add dynamic input keys)
    /// </summary>
    public void handleControl()
    {
        if (collision.above || collision.below)
            velocity.y = 0;

        Vector2 inputValue = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        SetSpriteDirection((int)inputValue.x);


        if (Input.GetKey(KeyCode.Space) && collision.below)
            velocity.y = jumpVelocity;

        velocity.x = inputValue.x * Speed;
        velocity.y += gravity * Time.deltaTime;
        
        Move(velocity * Time.deltaTime);
    }

}

