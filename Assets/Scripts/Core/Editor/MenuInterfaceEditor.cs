﻿using System;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MenuInterface))]
public class MenuInterfaceEditor : Editor
{
    GUIStyle titleStyle;

    public override void OnInspectorGUI()
    {
        titleStyle = new GUIStyle();
        titleStyle.fontSize = 14;
        titleStyle.fontStyle = FontStyle.Bold;
        titleStyle.normal.textColor = Color.gray;

        MenuInterface menu = (MenuInterface)target;
        
        EditorGUILayout.LabelField("Player :", titleStyle);
        menu.playerHealth = EditorGUILayout.IntField("Health", menu.playerHealth);
        menu.playerSpeed = EditorGUILayout.FloatField("Speed", menu.playerSpeed);
        menu.playerJumpPower = EditorGUILayout.FloatField("Jump Power", menu.playerJumpPower);
        EditorGUILayout.Separator();

        EditorGUILayout.LabelField("Camera : ", titleStyle);
        menu.cameraSmoothness = EditorGUILayout.Vector2Field("Smoothness", menu.cameraSmoothness);
        menu.cameraOffset = EditorGUILayout.Vector2Field("Offset", menu.cameraOffset);
        EditorGUILayout.Separator();

        EditorGUILayout.LabelField("Chunk : ", titleStyle);
        menu.chunkSize = EditorGUILayout.IntField("Size", menu.chunkSize);
        EditorGUILayout.Separator();

        EditorGUILayout.LabelField("Controller : ", titleStyle);
        menu.verticalRayCount = EditorGUILayout.IntField("Vertical Ray count", menu.verticalRayCount);
        menu.horizontalRayCount = EditorGUILayout.IntField("Horizontal Ray count", menu.horizontalRayCount);
        EditorGUILayout.Separator();
    }

}

